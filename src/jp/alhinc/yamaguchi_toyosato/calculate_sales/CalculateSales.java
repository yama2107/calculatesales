package jp.alhinc.yamaguchi_toyosato.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか確認（エラー処理）
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityNames = new HashMap<String, String>();
		Map<String, Long> commoditySales = new HashMap<String, Long>();

		//読み込み用メソッドの呼び出し
		if(!(inputFile(args[0], "branch.lst", "支店", "[0-9]{3}", branchNames, branchSales))) {
			return;
		}
		if(!(inputFile(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]{8}$", commodityNames, commoditySales))) {
			return;
		}

	//売上金額の集計
		//売上ファイルの抽出
		BufferedReader br = null;
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			if ( files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルが連番になっているか確認（エラー処理）
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i++) {
			//売上ファイルを読み込んで追加
			try {
				ArrayList<String>saleFile = new ArrayList<String>();
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				while((line = br.readLine()) != null) {
					saleFile.add(line);
				}

				//売上ファイルのフォーマットを確認（エラー処理）
				if(saleFile.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在するかの確認（エラー処理）
				if(!(branchNames.containsKey(saleFile.get(0)))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//売上ファイルの売上金額が数字なのか確認（エラー処理）
				if(!(commodityNames.containsKey(saleFile.get(1)))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}
				if(!(saleFile.get(2).matches("[0-9]*"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long fileSale = Long.parseLong(saleFile.get(2));
				Long saleAmount = branchSales.get(saleFile.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(saleFile.get(1)) + fileSale;

				//売上金額の合計が１０桁を超えているか確認（エラー処理）
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(saleFile.get(0), saleAmount);
				commoditySales.put(saleFile.get(1), commoditySaleAmount);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//出力用メソッドの呼び出し
		if(!(outputFile(args[0], "branch.out", branchNames, branchSales))) {
			return;
		}
		if(!(outputFile(args[0], "commodity.out", commodityNames, commoditySales))) {
			return;
		}
	}

	//読み込み用メソッド
	public static boolean inputFile(String directoryPath, String fileName, String index, String regex, Map<String,String> nameMap, Map<String,Long> saleMap) {
		File inputFile = new File(directoryPath, fileName);
		if(!(inputFile.exists())) {
			System.out.println(index + "定義ファイルが存在しません");
			return false;
		}
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(inputFile);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!(items[0].matches(regex)))) {
					System.out.println(index + "定義ファイルのフォーマットが不正です");
					return false;
				}
				nameMap.put(items[0], items[1]);
				saleMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//出力用メソッド
	public static boolean outputFile(String directoryPath, String fileName, Map<String, String> nameMap, Map<String, Long> saleMap) {
		BufferedWriter bw = null;
		try {
			File outputFile = new File(directoryPath, fileName);
			FileWriter fw = new FileWriter(outputFile);
			bw = new BufferedWriter(fw);
			for(String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
